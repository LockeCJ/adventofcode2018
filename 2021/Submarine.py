__author__ = 'LockeCJ'

from Sonar import Sonar
from Navigator import Navigator 
from AimNavigator import AimNavigator
from Diagnostic import Diagnostic
from Bingo import Bingo

class Submarine:
    
    sonar = Sonar()
    navigator = Navigator()
    aim_navigator = AimNavigator()
    diagnostic = Diagnostic()
    #bingo = Bingo()

    def __init__(self):
        pass

def main():
    sub = Submarine()
    with open('./2021/day1_input.txt') as f:
        measurements = f.read().splitlines()
        c = sub.sonar.count_increases(map(int, measurements))
        print(f'Out of {len(measurements)}, {c} measurements are larger than the previous.')
        c2 = sub.sonar.count_increases_window(map(int, measurements))
        print(f'Out of {len(measurements)}, {c2} measurements are larger than the previous.')

    with open('./2021/day2_input.txt') as f:
        chart = f.read().splitlines()
        sub.navigator.navigate(chart)
        print(f'Ending position: {sub.navigator.position}')
        print(f'Ending checksum: {sub.navigator.checksum()}')
        sub.aim_navigator.navigate(chart)
        print(f'(Aim)Ending position: {sub.aim_navigator.position}')
        print(f'(Aim)Ending checksum: {sub.aim_navigator.checksum()}')

    with open('./2021/day3_input.txt') as f:
        report = f.read().splitlines()
        sub.diagnostic.diagnose(report)
        print(f'Power consumption: {sub.diagnostic.power_consumption}')
        print(f'Life Support Rating: {sub.diagnostic.life_support_rating}')

    # with open('./2021/day4_input.txt') as f:
    #     game = f.read().splitlines()
    #     timeline = game[0]
    #     card_data = filter(lamda x: x<>'', game)
    #     #Need to create a card list of lists by merging 5 lists delimitted by spaces, repeat until all cards loaded
    #     sub.bingo.add_cards(cards)
    #     print(f'Power consumption: {sub.diagnostic.power_consumption}')
    #     print(f'Life Support Rating: {sub.diagnostic.life_support_rating}')

if __name__ == '__main__':
    main()