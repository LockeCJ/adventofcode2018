__author__ = 'LockeCJ'

from BingoCard import BingoCard

class Bingo:
    cards = []

    def __init__(self, cards) -> None:
        self.add_cards(cards)
        
    def add_cards(self, cards):
        for card in cards:
            self.cards.append(BingoCard(card))

    def find_winning_board(self, timeline):
        for play in range(0,len(timeline)):
            for card in self.cards:
                if card.validate(timeline[:play]):
                    return self.cards.index(card), timeline[:play]

    def calculate_winning_score(self, index, timeline):
        return self.cards[index].calculate_score(timeline)
