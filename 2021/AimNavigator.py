__author__ = 'LockeCJ'

class AimNavigator:
    position = 0,0,0
    
    def set_position(self, x, y, aim):
        self.position = x, y, aim

    def reset(self):
        self.set_position(0, 0, 0)

    def forward(self, magnitude):
        x = self.position[0] + int(magnitude)
        aim = self.position[2]
        y = self.position[1] + aim * int(magnitude) 
        self.set_position(x, y, aim)
    
    def up(self, magnitude):
        aim = self.position[2] - int(magnitude)
        self.set_position(self.position[0], self.position[1], aim)

    def down(self, magnitude):
        aim = self.position[2] + int(magnitude)
        self.set_position(self.position[0], self.position[1], aim)

    def checksum(self):
        return self.position[0] * self.position[1]
    
    def navigate(self, chart):
        for heading in chart:
            direction, magnitude = heading.split()
            if direction == 'forward':
                self.forward(magnitude)
            if direction == 'up':
                self.up(magnitude)
            if direction == 'down':
                self.down(magnitude)