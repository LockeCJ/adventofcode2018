__author__ = 'LockeCJ'

import Utility

class BingoCard:
    card = []

    def __init__(self, card) -> None:
        self.card = card

    def validate(self, timeline):
        for row in self.card:
            if set(row).issubset(timeline):
                return True
        for col in Utility.get_columns(self.card):
            if set(col).issubset(timeline):
                return True
        return False

    def calculate_score(self, timeline):
        score = 0
        for row in self.card:
            for item in row:
                if item not in timeline:
                    score += int(item)
        return score * int(timeline[-1])
