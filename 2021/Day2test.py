__author__ = 'LockeCJ'

import unittest
from Navigator import Navigator
from AimNavigator import AimNavigator

class Day2(unittest.TestCase):

    def test_day2_part1(self):
        n = Navigator()
        self.assertIsNotNone(n)
        n.reset()
        self.assertEqual(n.position, (0,0))
        n.forward(5)
        self.assertEqual(n.position, (5,0))
        n.down(8)
        self.assertEqual(n.position, (5,8))
        n.up(5)
        self.assertEqual(n.position, (5,3))
        sample_chart = ['forward 5', 'down 5', 'forward 8', 'up 3', 'down 8', 'forward 2']
        n.reset()
        n.navigate(sample_chart)
        self.assertEqual(n.position, (15,10))
        self.assertEqual(n.checksum(), 150)

    def test_day2_part2(self):
        a = AimNavigator()
        self.assertIsNotNone(a)
        a.reset()
        self.assertEqual(a.position, (0,0,0))
        a.forward(5)
        self.assertEqual(a.position, (5,0,0))
        a.down(8)
        self.assertEqual(a.position, (5,0,8))
        a.up(5)
        self.assertEqual(a.position, (5,0,3))
        a.forward(2)
        self.assertEqual(a.position, (7,6,3))
        sample_chart = ['forward 5', 'down 5', 'forward 8', 'up 3', 'down 8', 'forward 2']
        a.reset()
        a.navigate(sample_chart)
        self.assertEqual(a.position, (15,60,10))
        self.assertEqual(a.checksum(), 900)