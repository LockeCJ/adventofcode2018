__author__ = 'LockeCJ'

import unittest
from Diagnostic import Diagnostic

class Day3(unittest.TestCase):

    def test_day3_part1(self):
        d = Diagnostic()
        self.assertIsNotNone(d)
        sample_report = ['00100','11110','10110','10111','10101','01111','00111','11100','10000','11001','00010','01010']
        self.assertEqual(d.find_mode([0,0,1,0,0]), '0')
        self.assertEqual(d.find_mode(sample_report[0]), '0')
        self.assertEqual(d.find_mode(sample_report[1]), '1')
        d.calculate_power_consumption(sample_report)
        self.assertEqual(d.gamma_rate, '10110')
        self.assertEqual(int(d.gamma_rate,2), 22)
        self.assertEqual(d.epsilon_rate, '01001')
        self.assertEqual(int(d.epsilon_rate,2), 9)
        self.assertEqual(d.power_consumption, 198)

    def test_day3_part2(self):
        d = Diagnostic()
        self.assertIsNotNone(d)
        sample_report = ['00100','11110','10110','10111','10101','01111','00111','11100','10000','11001','00010','01010']
        d.calculate_life_support_rating(sample_report)
        self.assertEqual(d.oxygen_generator_rating, '10111')
        self.assertEqual(int(d.oxygen_generator_rating,2), 23)
        self.assertEqual(d.co2_scrubber_rating, '01010')
        self.assertEqual(int(d.co2_scrubber_rating,2), 10)
        self.assertEqual(d.life_support_rating, 230)
