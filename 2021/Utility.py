__author__ = 'LockeCJ'

def get_column(index, matrix):
    return map(lambda row: row[index], matrix)

def get_columns(matrix):
    for col in range(0,len(matrix[0])):
        yield get_column(col, matrix)
    
