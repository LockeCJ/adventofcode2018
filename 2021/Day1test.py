__author__ = 'LockeCJ'

import unittest
from Sonar import Sonar
class Day1(unittest.TestCase):

    def test_day1_part1(self):
        s = Sonar()
        self.assertIsNotNone(s)
        sample_measurements = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]
        c = s.count_increases(sample_measurements)
        self.assertEqual(c, 7)

    def test_day1_part2(self):
        s = Sonar()
        self.assertIsNotNone(s)
        sample_measurements = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]
        c = s.count_increases_window(sample_measurements)
        self.assertEqual(c, 5)
