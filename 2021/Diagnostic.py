__author__ = 'LockeCJ'

import Utility

class Diagnostic:
    gamma_rate = ''
    epsilon_rate = ''
    power_consumption = 0
    oxygen_generator_rating = ''
    co2_scrubber_rating = ''
    life_support_rating = 0

    def diagnose(self, report):
        self.calculate_power_consumption(report)
        self.calculate_life_support_rating(report)

    def calculate_power_consumption(self, report):
        for column in Utility.get_columns(report):
            self.gamma_rate += self.find_mode(list(column))
        self.epsilon_rate = ''.join(map(self.invert, self.gamma_rate))
        self.power_consumption = int(self.gamma_rate, 2) * int(self.epsilon_rate, 2)

    def calculate_life_support_rating(self, report):
        self.oxygen_generator_rating = self.filter_report(self.find_mode, report)[0]
        self.co2_scrubber_rating = self.filter_report(self.find_antimode, report)[0]
        self.life_support_rating = int(self.oxygen_generator_rating,2) * int(self.co2_scrubber_rating,2)

    def filter_report(self, match_function, report, index=0):
        match_value = match_function(list(Utility.get_column(index,report)))
        subreport = list(filter(lambda l: l[index] == match_value, report))
        return subreport if len(subreport) == 1 else self.filter_report(match_function, subreport, index+1)

    def find_mode(self, a):
        return '1' if sum(map(lambda x: x == '1', a)) >= len(a) /2 else '0'
    
    def find_antimode(self, a):
        return self.invert(self.find_mode(a))

    def invert(self, b):
        return '1' if b == '0' else '0'