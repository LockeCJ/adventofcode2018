__author__ = 'LockeCJ'

class Navigator:
    position = 0,0
    
    def set_position(self, x, y):
        self.position = x, y

    def reset(self):
        self.set_position(0, 0)

    def forward(self, magnitude):
        x = self.position[0] + int(magnitude)
        self.set_position(x, self.position[1])
    
    def up(self, magnitude):
        y = self.position[1] - int(magnitude)
        self.set_position(self.position[0], y)

    def down(self, magnitude):
        y = self.position[1] + int(magnitude)
        self.set_position(self.position[0], y)

    def checksum(self):
        return self.position[0] * self.position[1]
    
    def navigate(self, chart):
        for heading in chart:
            direction, magnitude = heading.split()
            if direction == 'forward':
                self.forward(magnitude)
            if direction == 'up':
                self.up(magnitude)
            if direction == 'down':
                self.down(magnitude)