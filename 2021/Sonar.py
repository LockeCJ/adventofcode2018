__author__ = 'LockeCJ'

from itertools import islice

class Sonar:

    def count_increases(self, measurements):
        previous_m = 999999 
        count = 0
        for m in measurements:
            if m > previous_m:
                count +=1
            previous_m = m
        return count
    
    def count_increases_window(self, measurements, window = 3):
        return self.count_increases(sum_window(measurements, window))

def window(seq, n=2):
    "Returns a sliding window (of width n) over data from the iterable"
    "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
    it = iter(seq)
    result = tuple(islice(it, n))
    if len(result) == n:
        yield result    
    for elem in it:
        result = result[1:] + (elem,)
        yield result

def sum_window(seq, n=2):
    for w in window(seq, n):
        yield sum(w)