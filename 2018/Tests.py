import unittest
from Day1 import Day1
from Day2 import Day2
from Day3 import Day3


class Tests(unittest.TestCase):
    def test_day1_part1(self):
        d = Day1()

        sample_input_1 = ['+1', '-2', '+3', '+1']
        self.assertEqual(d.find_frequency(sample_input_1), 3, 'Anomaly Detected!')
        sample_input_2 = ['+1', '+1', '+1']
        self.assertEqual(d.find_frequency(sample_input_2), 3, 'Anomaly Detected!')
        sample_input_3 = ['+1', '+1', '-2']
        self.assertEqual(d.find_frequency(sample_input_3), 0, 'Anomaly Detected!')
        sample_input_4 = ['-1', '-2', '-3']
        self.assertEqual(d.find_frequency(sample_input_4), -6, 'Anomaly Detected!')
        with open('day1_input.txt') as f:
            day1_input = f.read().splitlines()
        print(d.find_frequency(day1_input))
        self.assertEqual(d.find_frequency(day1_input), 513, 'Anomaly Detected!')
        with open('day1_input_alt.txt') as f:
            day1_input = f.read().splitlines()
        print(d.find_frequency(day1_input))
        # self.assertEqual(d.find_frequency(day1_input), 513, 'Anomaly Detected!')

    def test_day1_part2(self):
        d = Day1()
        sample_input_1 = ['+1', '-2', '+3', '+1']
        self.assertEqual(2, d.find_frequency_match(sample_input_1), 'No Match Found!')
        sample_input_2 = ['+1', '-1']
        self.assertEqual(0, d.find_frequency_match(sample_input_2), 'No Match Found!')
        sample_input_3 = ['+3', '+3', '+4', '-2', '-4']
        self.assertEqual(10, d.find_frequency_match(sample_input_3), 'No Match Found!')
        sample_input_4 = ['-6', '+3', '+8', '+5', '-6']
        self.assertEqual(5, d.find_frequency_match(sample_input_4), 'No Match Found!')
        sample_input_5 = ['+7', '+7', '-2', '-7', '-4']
        self.assertEqual(14, d.find_frequency_match(sample_input_5), 'No Match Found!')
        with open('day1_input.txt') as f:
            day1_input = f.read().splitlines()
        print(d.find_frequency_match(day1_input))
        self.assertEqual(287, d.find_frequency_match(day1_input), 'No Match Found!')
        with open('day1_input_alt.txt') as f:
            day1_input = f.read().splitlines()
        print(d.find_frequency_match(day1_input))
        self.assertEqual(70357, d.find_frequency_match(day1_input), 'No Match Found!')

    def test_day2_part1(self):
        d = Day2()
        sample_input = ['abcdef', 'bababc', 'abbcde', 'abcccd', 'aabcdd', 'abcdee', 'ababab']
        self.assertEqual(12, d.find_checksum(sample_input))
        with open('day2_input.txt') as f:
            day2_input = f.read().splitlines()
        print(d.find_checksum(day2_input))
        self.assertEqual(6696, d.find_checksum(day2_input))
        with open('day2_input_alt.txt') as f:
            day2_input = f.read().splitlines()
        print(d.find_checksum(day2_input))
        self.assertEqual(6642, d.find_checksum(day2_input))

    def test_day2_part2(self):
        d = Day2()
        sample_input = ['abcde', 'fghij', 'klmno', 'pqrst', 'fguij', 'axcye', 'wvxyz']
        # print(d.find_nearest_match(sample_input))
        self.assertEqual('fgij', d.find_common_letters(d.find_nearest_match(sample_input)))
        with open('day2_input.txt') as f:
            day2_input = f.read().splitlines()
        print(d.find_common_letters(d.find_nearest_match(day2_input)))
        self.assertEqual('bvnfawcnyoeyudzrpgslimtkj', d.find_common_letters(d.find_nearest_match(day2_input)))
        with open('day2_input_alt.txt') as f:
            day2_input = f.read().splitlines()
        print(d.find_common_letters(d.find_nearest_match(day2_input)))
        self.assertEqual('cvqlbidheyujgtrswxmckqnap', d.find_common_letters(d.find_nearest_match(day2_input)))

    def test_day3_part1(self):
        d = Day3()
        sample_input = ['#1 @ 1,3: 4x4', '#2 @ 3,1: 4x4', '#3 @ 5,5: 2x2']
        self.assertEqual(4, d.find_overlapping_area(sample_input))
        with open('day3_input.txt') as f:
            day3_input = f.read().splitlines()
        result = d.find_overlapping_area(day3_input)
        print(result)
        self.assertEqual(117505, result)
        result = d.find_non_overlapping_area(day3_input)
        print(result)
        self.assertEqual('1254', result)
        with open('day3_input_alt.txt') as f:
            day3_input = f.read().splitlines()
        result = d.find_overlapping_area(day3_input)
        print(result)
        self.assertEqual(110827, result)
        result = d.find_non_overlapping_area(day3_input)
        print(result)
        self.assertEqual('116', result)
