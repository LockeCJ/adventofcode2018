__author__ = 'LockeCJ'
'''
--- Day 3: No Matter How You Slice It ---

The Elves managed to locate the chimney-squeeze prototype fabric for Santa's suit (thanks to someone who helpfully wrote
 its box IDs on the wall of the warehouse in the middle of the night). Unfortunately, anomalies are still affecting them
  - nobody can even agree on how to cut the fabric.

The whole piece of fabric they're working on is a very large square - at least 1000 inches on each side.

Each Elf has made a claim about which area of fabric would be ideal for Santa's suit. All claims have an ID and consist 
of a single rectangle with edges parallel to the edges of the fabric. Each claim's rectangle is defined as follows:

    The number of inches between the left edge of the fabric and the left edge of the rectangle.
    The number of inches between the top edge of the fabric and the top edge of the rectangle.
    The width of the rectangle in inches.
    The height of the rectangle in inches.

A claim like #123 @ 3,2: 5x4 means that claim ID 123 specifies a rectangle 3 inches from the left edge, 2 inches from 
the top edge, 5 inches wide, and 4 inches tall. Visually, it claims the square inches of fabric represented by # (and 
ignores the square inches of fabric represented by .) in the diagram below:

...........
...........
...#####...
...#####...
...#####...
...#####...
...........
...........
...........

The problem is that many of the claims overlap, causing two or more claims to cover part of the same areas. For example,
 consider the following claims:

#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2

Visually, these claim the following areas:

........
...2222.
...2222.
.11XX22.
.11XX22.
.111133.
.111133.
........

The four square inches marked with X are claimed by both 1 and 2. (Claim 3, while adjacent to the others, does not 
overlap either of them.)

If the Elves all proceed with their own plans, none of them will have enough fabric. How many square inches of fabric 
are within two or more claims?
'''
'''
--- Part Two ---

Amidst the chaos, you notice that exactly one claim doesn't overlap by even a single square inch of fabric with any other claim. If you can somehow draw attention to it, maybe the Elves will be able to make Santa's suit after all!

For example, in the claims above, only claim 3 is intact after all claims are made.

What is the ID of the only claim that doesn't overlap?

'''

import re


class Day3:

    def parse_rectangle(self, rect):
        match = re.match('#(\d{1,4})\s@\s(\d{1,4}),(\d{1,4}):\s(\d{1,4})x(\d{1,4})', rect)
        rectangle = {
            'id': match.group(1),
            'x': match.group(2),
            'y': match.group(3),
            'width': match.group(4),
            'height': match.group(5)
        }
        return rectangle

    def find_overlapping_area(self, rects):
        rectangles = self.get_rectangles(rects)
        fabric = self.fill_fabric(rectangles)
        total = 0
        for y in range(len(fabric[0])):
            for x in range(len(fabric)):
                if fabric[x][y] > 1:
                    total += 1
        return total

    def fill_fabric(self, rectangles):
        fabric = [[0 for i in range(1000)] for j in range(1000)]
        for rectangle in rectangles:
            # Add rectangle to grid
            start_x = int(rectangle['x'])
            start_y = int(rectangle['y'])
            width = int(rectangle['width'])
            height = int(rectangle['height'])
            for y in range(start_y, start_y + height):
                for x in range(start_x, start_x + width):
                    fabric[x][y] += 1
        return fabric

    def get_rectangles(self, rects):
        rectangles = [self.parse_rectangle(r) for r in rects]
        return rectangles

    def find_non_overlapping_area(self, rects):
        rectangles = self.get_rectangles(rects)
        fabric = self.fill_fabric(rectangles)
        for rectangle in rectangles:
            # Check rectangle for overlap
            start_x = int(rectangle['x'])
            start_y = int(rectangle['y'])
            width = int(rectangle['width'])
            height = int(rectangle['height'])
            non_overlap_total = 0
            for y in range(start_y, start_y + height):
                for x in range(start_x, start_x + width):
                    if fabric[x][y] == 1:
                        non_overlap_total += 1
            if non_overlap_total == width * height:
                return rectangle['id']