import unittest
import math
from ExpenseReport import ExpenseReport

class Tests(unittest.TestCase):

    def test_day1_part1(self):
        er = ExpenseReport()

        sample_input_1 = [1721,979,366,299,675,1456]
        c = er.find_expenses(sample_input_1, 2, 2020)
        self.assertEqual(c[0], 1721)
        self.assertEqual(c[1], 299)
        self.assertEqual(math.prod(c), 514579)
        with open('./2020/day1_input.txt') as f:
            day1_input = f.read().splitlines()
        c = er.find_expenses(map(int, day1_input), 2, 2020)
        self.assertIsNotNone(c, "No expense pair found!")
        print(c)
        print(math.prod(c))

    def test_day1_part2(self):
        er = ExpenseReport()

        sample_input_1 = [1721,979,366,299,675,1456]
        c = er.find_expenses(sample_input_1, 3, 2020)
        self.assertIsNotNone(c, "No expense set found!")
        self.assertEqual(c[0], 979)
        self.assertEqual(c[1], 366)
        self.assertEqual(c[2], 675)
        self.assertEqual(math.prod(c), 241861950)
        with open('./2020/day1_input.txt') as f:
            day1_input = f.read().splitlines()
        c = er.find_expenses(map(int, day1_input), 3, 2020)
        self.assertIsNotNone(c, "No expense set found!")
        print(c)
        print(math.prod(c))
