import unittest
from PasswordValidator import PasswordValidator

class Tests(unittest.TestCase):

    def test_day2_part1(self):
        samplePasswordData = ['1-3 a: abcde','1-3 b: cdefg','2-9 c: ccccccccc']
        count = 0
        for pd in samplePasswordData:
            pv = PasswordValidator(pd)
            if pv.validate_count():
                count = count + 1
        self.assertEqual(count, 2)
        with open('./2020/day2_input.txt') as f:
            day2_input = f.read().splitlines()
        count = 0
        for pd in day2_input:
            pv = PasswordValidator(pd)
            if pv.validate_count():
                count = count + 1
        print(f'\r\nCount: {count}')
        
    def test_day2_part2(self):
        samplePasswordData = ['1-3 a: abcde','1-3 b: cdefg','2-9 c: ccccccccc']
        count = 0
        for pd in samplePasswordData:
            pv = PasswordValidator(pd)
            if pv.validate_position():
                count = count + 1
        self.assertEqual(count, 1)
        with open('./2020/day2_input.txt') as f:
            day2_input = f.read().splitlines()
        count = 0
        for pd in day2_input:
            pv = PasswordValidator(pd)
            if pv.validate_position():
                count = count + 1
        print(f'\r\nCount: {count}')
