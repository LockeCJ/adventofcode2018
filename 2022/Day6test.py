__author__ = 'LockeCJ'

import unittest

from ElfCommunicator import ElfCommunicator

sample_signals = ["mjqjpqmgbljsphdztnvjfqwrcgsmlb", "bvwbjplbgvbhsrlpgdmjqwftvncz", "nppdvjthqldpwncqszvftbrmjlhg", "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"]
communicator = ElfCommunicator()

class Day6(unittest.TestCase):
    def test_day6_part1(self):
        self.assertEqual(communicator.find_packet_marker(sample_signals[0]),7)
        self.assertEqual(communicator.find_packet_marker(sample_signals[1]),5)
        self.assertEqual(communicator.find_packet_marker(sample_signals[2]),6)
        self.assertEqual(communicator.find_packet_marker(sample_signals[3]),10)
        self.assertEqual(communicator.find_packet_marker(sample_signals[4]),11)

    def test_day6_part2(self):
        self.assertEqual(communicator.find_message_marker(sample_signals[0]),19)
        self.assertEqual(communicator.find_message_marker(sample_signals[1]),23)
        self.assertEqual(communicator.find_message_marker(sample_signals[2]),23)
        self.assertEqual(communicator.find_message_marker(sample_signals[3]),29)
        self.assertEqual(communicator.find_message_marker(sample_signals[4]),26)