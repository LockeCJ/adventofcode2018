__author__ = 'LockeCJ'

class ElfCommunicator:
    def find_marker(self, signal, marker_length):
        #start scanning signal
        for start in range(0, len(signal) - marker_length):
            check = set([c for c in signal[start:start+marker_length]])
            if len(check) == marker_length:
                return start + marker_length

    def find_packet_marker(self, signal):
        return self.find_marker(signal, 4)
    
    def find_message_marker(self, signal):
        return self.find_marker(signal, 14)