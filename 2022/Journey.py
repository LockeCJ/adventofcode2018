__author__ = 'LockeCJ'

from CalorieCounter import CalorieCounter
from CrateMover import CrateMover9000, CrateMover9001
from ElfCommunicator import ElfCommunicator
from ElfFileSystem import ElfFileSystem
from JobAssignmentAnalyzer import JobAssignmentAnalyzer
from RockPaperScissors import RockPaperScissors
from RuckSackChecker import RuckSackChecker

from rich import print

from TreeMapper import TreeMapper

class Journey:
    
    calorie_counter = CalorieCounter()
    rock_paper_scissors = RockPaperScissors()
    rucksack_checker = RuckSackChecker()
    job_assignment_analyser = JobAssignmentAnalyzer()
    crate_mover = None
    communicator = ElfCommunicator()
    file_system = None
    tree_mapper = None

    def __init__(self):
        pass

def main():
    j = Journey()
    print("Starting Journey...")
    #Day 1
    start_day(1)
    with open('./2022/day1_input.txt') as f:
        calories = f.read().splitlines()
    print("Starting up Calorie Counter...")
    print(f'Maximum calories carried by an Elf: {j.calorie_counter.max_calories(calories)}')
    print(f'Total calories carried by the top 3 Elves: {sum(j.calorie_counter.top_n_calories(calories,3))}')
    #Day 2
    start_day(2)
    with open('./2022/day2_input.txt') as f:
        plays = [match.split(' ') for match in f.read().splitlines()]
    print("Starting up Rock Paper Scissors Analyzer...")
    print(f'Total score after naieve analysis: {j.rock_paper_scissors.total_score_naievely(plays)}')
    print(f'Total score after correct analysis: {j.rock_paper_scissors.total_score_correctly(plays)}')
    #Day 3
    start_day(3)
    with open('./2022/day3_input.txt') as f:
        sacks = f.read().splitlines()
    print("Starting up Rucksack Checker...")
    print(f"Total Item priorities: {j.rucksack_checker.check_sacks(sacks)}")
    print(f"Total Badge priorities: {j.rucksack_checker.check_badges(sacks)}")
    #Day 4
    start_day(4)
    with open('./2022/day4_input.txt') as f:
        pairs = [line.split(',') for line in f.read().splitlines()]
    print("Starting up Job Assignment Analyzer...")
    print(f"Total Pairs fully contained: {j.job_assignment_analyser.analyze_pairs_and(pairs)}")
    print(f"Total Pairs overlapping: {j.job_assignment_analyser.analyze_pairs_or(pairs)}")
    #Day 5
    start_day(5)
    with open('./2022/day5_input.txt') as f:
        lines = f.read().splitlines()
        cargo_loaded = False
        cargo = list()
        instructions = list()
        for line in lines:
            if line == '':
                cargo_loaded = True
                continue
            if not cargo_loaded:
                cargo.append(line)
            else:
                instructions.append(line)
    print("Starting up Crate Mover...")
    j.crate_mover = CrateMover9000(cargo)
    j.crate_mover.process_instructions(instructions)
    print(f"Cargo at the top of the stacks(9000): {j.crate_mover.top_boxes()}")
    j.crate_mover = CrateMover9001(cargo)
    j.crate_mover.process_instructions(instructions)
    print(f"Cargo at the top of the stacks(9001): {j.crate_mover.top_boxes()}")
    #Day 6
    start_day(6)
    with open('./2022/day6_input.txt') as f:
        signals = f.read().splitlines()
    print("Starting up Elf Communicator...")
    for signal in signals:
        print(f"Packet marker found after {j.communicator.find_packet_marker(signal)} characters.")
        print(f"Message marker found after {j.communicator.find_message_marker(signal)} characters.")
    #Day 7
    start_day(7)
    with open('./2022/day7_input.txt') as f:
        commands = f.read().splitlines()
    print("Starting up Elf File System Analyzer...")
    j.file_system = ElfFileSystem(commands)
    print(f"Sum of the total size of the directories with a size of at most 100000: {j.file_system.total_size_by_max_size(100000)}")
    print(f"total size of the smallest directory that can be deleted to free up enough space for the update: {j.file_system.total_size_of_dir_to_delete(70000000, 30000000)}")
    #Day 8
    start_day(8)
    with open('./2022/day8_input.txt') as f:
        tree_lines = f.read().splitlines()
    print("Starting up Tree Mapper...")
    j.tree_mapper = TreeMapper(tree_lines)    
    print(f"Total number of trees visible: {j.tree_mapper.total_trees_visible()}")
    print(f"Maximum scenic score possible: {j.tree_mapper.max_scenic_score()}")

def start_day(day):
    print(f":calendar: Day {day}")

if __name__ == '__main__':
    main()