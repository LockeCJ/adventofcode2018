__author__ = 'LockeCJ'

import unittest
from CrateMover import CrateMover9000, CrateMover9001

sample_cargo = ['    [D]    ','[N] [C]    ','[Z] [M] [P]']
sample_instructions = ['move 1 from 2 to 1','move 3 from 1 to 3','move 2 from 2 to 1','move 1 from 1 to 2']

class Day5(unittest.TestCase):
    def test_day5_part1(self):
        cm1 = CrateMover9000(sample_cargo)
        self.assertEqual(cm1.top_boxes(), 'NDP')
        cm1.load_instructions(sample_instructions)
        cm1.process_instruction()
        self.assertEqual(cm1.top_boxes(), 'DCP')
        cm1.process_instruction()
        self.assertEqual(cm1.top_boxes(), ' CZ')
        cm1.process_instruction()
        self.assertEqual(cm1.top_boxes(), 'M Z')
        cm1.process_instruction()
        self.assertEqual(cm1.top_boxes(), 'CMZ')

    def test_day5_part2(self):
        cm1 = CrateMover9001(sample_cargo)
        self.assertEqual(cm1.top_boxes(), 'NDP')
        cm1.load_instructions(sample_instructions)
        cm1.process_instruction()
        self.assertEqual(cm1.top_boxes(), 'DCP')
        cm1.process_instruction()
        self.assertEqual(cm1.top_boxes(), ' CD')
        cm1.process_instruction()
        self.assertEqual(cm1.top_boxes(), 'C D')
        cm1.process_instruction()
        self.assertEqual(cm1.top_boxes(), 'MCD')
