__author__ = 'LockeCJ'

import unittest
from RockPaperScissors import RockPaperScissors
class Day2(unittest.TestCase):

    rps = RockPaperScissors()
    sample_plays = [['A','Y'], ['B','X'], ['C','Z']]

    def test_day2_part1(self):
        self.assertEqual(self.rps.total_score_naievely(self.sample_plays), 15)

    def test_day2_part2(self):
        self.assertEqual(self.rps.total_score_correctly(self.sample_plays), 12)

    def test_score_item(self):
        self.assertEqual(self.rps.score_item('A'), 1)
        self.assertEqual(self.rps.score_item('B'), 2)
        self.assertEqual(self.rps.score_item('C'), 3)

    def test_score_play(self):
        self.assertEqual(self.rps.score_play('A', 'A'), 3)
        self.assertEqual(self.rps.score_play('A', 'B'), 0)
        self.assertEqual(self.rps.score_play('A', 'C'), 6)
        self.assertEqual(self.rps.score_play('B', 'A'), 6)        
        self.assertEqual(self.rps.score_play('B', 'B'), 3)
        self.assertEqual(self.rps.score_play('B', 'C'), 0)
        self.assertEqual(self.rps.score_play('C', 'A'), 0)        
        self.assertEqual(self.rps.score_play('C', 'B'), 6)
        self.assertEqual(self.rps.score_play('C', 'C'), 3)
    
    def test_score_naievely(self):
        self.assertEqual(self.rps.score_naievely(['A','X']), 4)
        self.assertEqual(self.rps.score_naievely(['B','X']), 1)
        self.assertEqual(self.rps.score_naievely(['C','X']), 7)
        self.assertEqual(self.rps.score_naievely(['A','Y']), 8)
        self.assertEqual(self.rps.score_naievely(['B','Y']), 5)
        self.assertEqual(self.rps.score_naievely(['C','Y']), 2)
        self.assertEqual(self.rps.score_naievely(['A','Z']), 3)
        self.assertEqual(self.rps.score_naievely(['B','Z']), 9)
        self.assertEqual(self.rps.score_naievely(['C','Z']), 6)

    def test_score_correctly(self):
        self.assertEqual(self.rps.score_correctly(['A','X']), 3)
        self.assertEqual(self.rps.score_correctly(['A','Y']), 4)
        self.assertEqual(self.rps.score_correctly(['A','Z']), 8)
        self.assertEqual(self.rps.score_correctly(['B','X']), 1)
        self.assertEqual(self.rps.score_correctly(['B','Y']), 5)
        self.assertEqual(self.rps.score_correctly(['B','Z']), 9)
        self.assertEqual(self.rps.score_correctly(['C','X']), 2)
        self.assertEqual(self.rps.score_correctly(['C','Y']), 6)
        self.assertEqual(self.rps.score_correctly(['C','Z']), 7)

    def test_play_naievely(self):
        self.assertEqual(self.rps.play_naievely('X'), 'A')        
        self.assertEqual(self.rps.play_naievely('Y'), 'B')        
        self.assertEqual(self.rps.play_naievely('Z'), 'C')
    
    def test_play_correctly(self):
        self.assertEqual(self.rps.play_correctly('A', 'X'), 'C')
        self.assertEqual(self.rps.play_correctly('A', 'Y'), 'A')
        self.assertEqual(self.rps.play_correctly('A', 'Z'), 'B')
        self.assertEqual(self.rps.play_correctly('B', 'X'), 'A')
        self.assertEqual(self.rps.play_correctly('B', 'Y'), 'B')
        self.assertEqual(self.rps.play_correctly('B', 'Z'), 'C')
        self.assertEqual(self.rps.play_correctly('C', 'X'), 'B')
        self.assertEqual(self.rps.play_correctly('C', 'Y'), 'C')
        self.assertEqual(self.rps.play_correctly('C', 'Z'), 'A')
