__author__ = 'LockeCJ'

import unittest

from ElfFileSystem import ElfFileSystem

sample_fs = ["$ cd /","$ ls","dir a","14848514 b.txt","8504156 c.dat","dir d","$ cd a","$ ls","dir e","29116 f","2557 g","62596 h.lst","$ cd e","$ ls","584 i","$ cd ..","$ cd ..","$ cd d","$ ls","4060174 j","8033020 d.log","5626152 d.ext","7214296 k"]
efs = ElfFileSystem(sample_fs)


class Day7(unittest.TestCase):
    def test_day7_part1(self):
        self.assertEqual(efs.total_size(efs.fs), 48381165)
        for a in efs.find_dirs(efs.fs):
            print(a)
        self.assertEqual(len(list(efs.find_dirs_by_max_size(100000))), 2)
        self.assertEqual(efs.total_size_by_max_size(100000), 95437)
    def test_day7_part2(self):
        self.assertEqual(efs.total_size_of_dir_to_delete(70000000, 30000000), 24933642)