__author__ = 'LockeCJ'

from collections import deque
import re

class CrateMover:
    def __init__(self, cargo) -> None:
        self.load_cargo(cargo)
        self.instructions = list()

    def load_cargo(self, cargo):
        self.cargo = list()
        temp_cargo = cargo.copy()
        temp_cargo.reverse()
        for r in temp_cargo:
            col = 0            
            for c in [r[i: i + 4] for i in range(0, len(r), 4)]:
                if len(self.cargo) == col:
                    self.cargo.append(deque())
                m = re.match('\[(.)\]',c)
                if m is not None:
                    self.cargo[col].append(m.group(1))
                col = col + 1

    def load_instructions(self, instructions):
        self.instructions = list()
        self.instruction_index = 0
        pat = "^move\s(\d+)\sfrom\s(\d+)\sto\s(\d+)$"
        for instruction in instructions:
            m = re.match(pat, instruction)
            self.instructions.append(m.groups())

    def process_instructions(self, instructions=None):
        if len(self.instructions) == 0:
            self.load_instructions(instructions)
        for instruction in self.instructions:
            self.process_instruction(instruction)
    
    def process_instruction(self, instruction=None):
        if instruction == None:
            instruction = self.instructions[self.instruction_index]
        quantity, source, destination = instruction
        self.move(int(quantity), int(source), int(destination))
        self.instruction_index = self.instruction_index + 1

    def move(self, quantity, source, destination):
        pass

    def top_box(self, stack):
        if len(stack) > 0: 
            return stack[-1] 
        else: 
            return " "

    def top_boxes(self):
        b = [self.top_box(c) for c in self.cargo]
        return ''.join(b)

class CrateMover9000(CrateMover):
    def move(self, quantity, source, destination):
        for i in range(0, quantity):
            self.cargo[destination-1].append(self.cargo[source-1].pop())

class CrateMover9001(CrateMover):
    def move(self, quantity, source, destination):
        temp = deque()
        for i in range(0, quantity):
            temp.append(self.cargo[source-1].pop())
        for i in range(0, quantity):
            self.cargo[destination-1].append(temp.pop())