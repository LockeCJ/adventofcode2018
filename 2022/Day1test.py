__author__ = 'LockeCJ'

import unittest
from CalorieCounter import CalorieCounter
class Day1(unittest.TestCase):

    def test_day1_part1(self):
        c = CalorieCounter()
        self.assertIsNotNone(c)
        sample_calories = ["1000","2000","3000","","4000","","5000","6000","","7000","8000","9000","","10000"]
        self.assertEqual(c.max_calories(sample_calories), 24000)

    def test_day1_part2(self):
        c = CalorieCounter()
        self.assertIsNotNone(c)
        sample_calories = ["1000","2000","3000","","4000","","5000","6000","","7000","8000","9000","","10000"]
        top_3_calories = c.top_n_calories(sample_calories, 3)
        self.assertEqual(sum(top_3_calories), 45000)
