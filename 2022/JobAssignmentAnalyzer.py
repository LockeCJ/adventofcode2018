__author__ = 'LockeCJ'

class JobAssignmentAnalyzer:
    def analyze_pairs_and(self, pairs):        
        count = 0
        for pair in pairs:
            if self.analyze_pair_and(pair):
                count = count + 1
        return count

    def analyze_pairs_or(self, pairs):        
        count = 0
        for pair in pairs:
            if self.analyze_pair_or(pair):
                count = count + 1
        return count

    def analyze_pair_and(self, pair):
        temp = [p.split('-') for p in pair]
        compare = [[int(j) for j in i] for i in temp]
        if self.is_between(compare[0][0], compare[1][0], compare[1][1]) and self.is_between(compare[0][1], compare[1][0], compare[1][1]):
            return True
        if self.is_between(compare[1][0], compare[0][0], compare[0][1]) and self.is_between(compare[1][1], compare[0][0], compare[0][1]):
            return True
        return False

    def analyze_pair_or(self, pair):
        temp = [p.split('-') for p in pair]
        compare = [[int(j) for j in i] for i in temp]
        if self.is_between(compare[0][0], compare[1][0], compare[1][1]) or self.is_between(compare[0][1], compare[1][0], compare[1][1]):
            return True
        if self.is_between(compare[1][0], compare[0][0], compare[0][1]) or self.is_between(compare[1][1], compare[0][0], compare[0][1]):
            return True
        return False

    def is_between(self, test, min, max):
        return min <= test <= max
