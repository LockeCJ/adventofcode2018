__author__ = 'LockeCJ'

import unittest
from RuckSackChecker import RuckSackChecker

sample_sacks = ['vJrwpWtwJgWrhcsFMMfFFhFp','jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL','PmmdzqPrVvPwwTWBwg','wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn','ttgJtRGJQctTZtZT','CrZsJsPPZsGzwwsLwLmpwMDw']

class Day3(unittest.TestCase):
    checker = RuckSackChecker()

    def test_day3_part1(self):
        self.assertEqual(self.checker.check_sacks(sample_sacks), 157)

    def test_day3_part2(self):
        self.assertEqual(self.checker.check_badges(sample_sacks), 70)

    def test_check_sack(self):
        self.assertEqual(self.checker.check_sack(sample_sacks[0]),16)
        self.assertEqual(self.checker.check_sack(sample_sacks[1]),38)
        self.assertEqual(self.checker.check_sack(sample_sacks[2]),42)
        self.assertEqual(self.checker.check_sack(sample_sacks[3]),22)
        self.assertEqual(self.checker.check_sack(sample_sacks[4]),20)
        self.assertEqual(self.checker.check_sack(sample_sacks[5]),19)
    
    def test_find_badge(self):
        self.assertEqual(self.checker.find_badge(sample_sacks[0:3]), 'r')
        self.assertEqual(self.checker.find_badge(sample_sacks[3:6]), 'Z')        