__author__ = 'LockeCJ'

import itertools

class CalorieCounter:
    def max_calories(self, calories_list):
        return max(self.sum_by_elf(calories_list))

    def top_n_calories(self, calories_list, quantity):
        return itertools.islice(sorted(self.sum_by_elf(calories_list),reverse=True), quantity)

    def sum_by_elf(self, calories_list):
        calories_by_elf = self.group_by_elf(calories_list)
        ints = [[int(j) for j in i] for i in calories_by_elf]
        return [sum(c) for c in ints]

    def group_by_elf(self, calories_list):
        return [list(group) for key, group in itertools.groupby(calories_list, lambda z: z == "") if not key]
