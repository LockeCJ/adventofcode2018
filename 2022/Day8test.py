__author__ = 'LockeCJ'

import unittest

from TreeMapper import TreeMapper

sample_trees = ['30373',
                '25512',
                '65332',
                '33549',
                '35390']
tm = TreeMapper(sample_trees)
class Day8(unittest.TestCase):
    def test_day8_part1(self):
        self.assertListEqual(tm.left(2, 0), [0,3])
        self.assertListEqual(tm.right(2, 0), [7,3])
        self.assertIsNone(tm.up(2,0))
        self.assertListEqual(tm.down(2,0), [5, 3, 5, 3])
        height = tm.height(2, 0)
        self.assertFalse(tm.is_visible(height, tm.left(2,0)))
        self.assertFalse(tm.is_visible(height, tm.right(2,0)))
        self.assertTrue(tm.is_visible(height, tm.up(2,0)))
        self.assertFalse(tm.is_visible(height, tm.down(2,0)))
        self.assertEquals(tm.total_trees_visible(), 21)
    def test_day8_part2(self):
        height = tm.height(2,1)
        self.assertEquals(tm.count_visible(height, tm.up(2,1)), 1)
        self.assertEquals(tm.count_visible(height, tm.down(2,1)), 2)
        self.assertEquals(tm.count_visible(height, tm.left(2,1)), 1)
        self.assertEquals(tm.count_visible(height, tm.right(2,1)), 2)
        self.assertEquals(tm.scenic_score(2, 1), 4)
        self.assertEquals(tm.count_visible(height, tm.up(2,3)), 2)
        self.assertEquals(tm.count_visible(height, tm.down(2,3)), 1)
        self.assertEquals(tm.count_visible(height, tm.left(2,3)), 2)
        self.assertEquals(tm.count_visible(height, tm.right(2,3)), 2)        
        self.assertEquals(tm.scenic_score(2, 3), 8)
        self.assertEquals(tm.max_scenic_score(), 8)