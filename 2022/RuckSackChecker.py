__author__ = 'LockeCJ'

priorities = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

class RuckSackChecker:
    def check_sacks(self, sacks):
        return sum([self.check_sack(s) for s in sacks])

    def check_sack(self, sack):
        compartment_one, compartment_two = sack[:len(sack)//2], sack[len(sack)//2:]
        common = set(compartment_one).intersection(compartment_two).pop()
        return self.get_priority(common)

    def get_priority(self, item):
        return priorities.index(item) + 1

    def check_badges(self, sacks):
        return sum(self.find_badges(sacks))

    def find_badges(self, sacks):
        for i in range(0,len(sacks),3):
            yield self.get_priority(self.find_badge(sacks[i:i+3]))

    def find_badge(self,group):
        return set(group[0]).intersection(group[1]).intersection(group[2]).pop()