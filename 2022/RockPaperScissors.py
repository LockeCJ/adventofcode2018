__author__ = 'LockeCJ'

class RockPaperScissors:
    def total_score_naievely(self, plays):
        scores = [self.score_naievely(p) for p in plays]
        return sum(scores)

    def total_score_correctly(self, plays):
        scores = [self.score_correctly(p) for p in plays]
        return sum(scores)

    def score_naievely(self, play):
        their_play = play[0]
        my_play = self.play_naievely(play[1])
        return self.score_play(my_play, their_play) + self.score_item(my_play)

    def score_correctly(self, play):
        their_play = play[0]
        my_play = self.play_correctly(their_play, play[1])
        return self.score_play(my_play, their_play) + self.score_item(my_play)

    def play_naievely(self, play):
        match play:
            case 'X':
                return 'A'
            case 'Y':
                return 'B'
            case 'Z':
                return 'C'

    def play_correctly(self, their_play, strategy):
        match strategy:
            case 'X': #Lose
                match their_play:
                    case 'A': #Rock
                        return 'C' #Scissors
                    case 'B': #Paper
                        return 'A' #Rock
                    case 'C': #Scissors
                        return 'B' #Paper
            case 'Y': #Draw
                return their_play
            case 'Z': #Win
                match their_play:
                    case 'A': #Rock
                        return 'B' #Paper
                    case 'B': #Paper
                        return 'C' #Scissors
                    case 'C': #Scissors
                        return 'A' #Rock

    def score_item(self, item):
        match item:
            case 'A': #Rock
                return 1
            case 'B': #Paper
                return 2
            case 'C': #Scissors
                return 3

    def score_play(self, my_play, their_play):
        match my_play:
            case 'A': #Rock
                match their_play:
                    case 'B': #Paper
                        return 0 #Lose
                    case 'C': #Scissors
                        return 6 #Win
            case 'B': #Paper
                match their_play:
                    case 'A': #Rock
                        return 6 #Win
                    case 'C': #Scissors
                        return 0 #Lose
            case 'C': #Scissors
                match their_play:
                    case 'A': #Rock
                        return 0 #Lose
                    case 'B': #Paper
                        return 6 #Win
        return 3 #Draw        
        # if my_play == 'A' and their_play == 'B': #Rock vs. Paper
        #     return 0 #Lose
        # if my_play == 'A' and their_play == 'C': #Rock vs. Scissors
        #     return 6 #Win
        # if my_play == 'B' and their_play == 'A': #Paper vs. Rock
        #     return 6 #Win
        # if my_play == 'B' and their_play == 'C': #Paper vs. Scissors
        #     return 0 #Lose
        # if my_play == 'C' and their_play == 'A': #Scissors vs. Rock
        #     return 0 #Lose
        # if my_play == 'C' and their_play == 'B': #Scissors vs. Paper
        #     return 6 #Win
