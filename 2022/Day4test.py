__author__ = 'LockeCJ'

import unittest
from JobAssignmentAnalyzer import JobAssignmentAnalyzer

analyzer = JobAssignmentAnalyzer()
sample_pairs= [['2-4','6-8'],['2-3','4-5'],['5-7','7-9'],['2-8','3-7'],['6-6','4-6'],['2-6','4-8']]

class Day4(unittest.TestCase):
    def test_day4_part1(self):
        self.assertEqual(analyzer.analyze_pairs_and(sample_pairs), 2)
        

    def test_day4_part2(self):
        self.assertEqual(analyzer.analyze_pairs_or(sample_pairs), 4)