__author__ = 'LockeCJ'

import itertools
import re

from collections import defaultdict

class ElfFileSystem:
    def __init__(self, commands) -> None:
        self.fs = defaultdict()
        self.reconstruct_filesystem(commands)

    def reconstruct_filesystem(self, commands):
        path = "/"
        self.fs[path] = defaultdict() #initialize root path
        for command in commands:
            path = self.process_command(path, command)

    def process_command(self, path, command):
        commands = {
            "^\$\sls$": "ls",
            "^\$\scd\s(.+)$": "cd" ,
            "^dir\s(.+)$": "dir",
            "^(\d+)\s(.+)$": "file"
        }
        cmd = None
        #Match the command and parse the relevant info
        for regex in commands.keys():
            cmd = re.match(regex, command)
            if cmd is not None:
                break
        #Branch on exactly which command was entered
        match commands[cmd.re.pattern]:
            #There's nothing to do for an 'ls' command
            case "cd":
                dirname = cmd.group(1)
                match dirname:
                    case "/": #Root                        
                        path = dirname
                    case "..": #Go Back
                        parts = path.split('/')
                        less_parts = parts[:-1]
                        path = "/".join(less_parts)
                    case _: #Go Forward
                        if path == "/":
                            path = f"/{dirname}"
                        else:
                            path = f"{path}/{dirname}"
            case "dir": #Add directory
                current = self.find_path(path)
                dirname = cmd.group(1)
                current[dirname] = defaultdict()
            case "file": #Add File
                current = self.find_path(path)
                filename = cmd.group(2)
                filesize = cmd.group(1)
                current[filename] = filesize
        return path
    
    def find_path(self, path):
        current = self.fs["/"]
        for p in path.split("/"):
            if p in current:
                current = current[p]
        return current

    def total_size(self, fs):
        total = 0
        for f in fs.keys():
            if type(fs[f]) is defaultdict: #Directory
                total = total + self.total_size(fs[f])
            else: #File
                total = total + int(fs[f])
        return total

    def total_size_by_max_size(self, max_size):
        return sum([self.total_size(d) for d in self.find_dirs_by_max_size(max_size)])

    def find_dirs(self, fs):
        for f in fs.keys():
            if type(fs[f]) is defaultdict:
                yield fs[f]
                yield from self.find_dirs(fs[f])

    def find_dirs_by_max_size(self, max_size):
        for d in self.find_dirs(self.fs):
            if self.total_size(d) <= max_size:
                yield d

    def find_dirs_by_min_size(self, min_size):
        for d in self.find_dirs(self.fs):
            if self.total_size(d) >= min_size:
                yield d

    def total_size_of_dir_to_delete(self, total_space, needed_space):
        #Find current available space
        used_space = self.total_size(self.fs)
        free_space = total_space - used_space
        remaining_needed_space = needed_space - free_space
        return min([self.total_size(d) for d in self.find_dirs_by_min_size(remaining_needed_space)])

#Basically a tree of dictionaries. 
# A file has the name as the key, and the size as the value. 
# A directory has the name as a key, and a dictionary of files and directories as a value. 
# Iterate through the dictionary, walking into each directory.
# Recursion will be needed.