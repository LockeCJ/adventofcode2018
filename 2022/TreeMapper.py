__author__ = 'LockeCJ'

import itertools
import re

from collections import defaultdict

class TreeMapper:
    def __init__(self, tree_lines) -> None:
        self.tree_lines = tree_lines

    def is_visible(self, height, tree_line):
        if tree_line is None:
            return True
        return max(tree_line) < height

    def count_visible(self, height, tree_line):
        if tree_line is None:
            return 0
        count = 0
        for t in tree_line:
            count = count + 1
            if t >= height:
                break
        return count
    
    def up(self, x, y):
        if y == 0:
            return None
        return [int(t[x]) for t in self.tree_lines[y-1::-1]]

    def down(self, x, y):
        if y == len(self.tree_lines) - 1:
            return None
        return [int(t[x]) for t in self.tree_lines[y+1:]]

    def left(self, x, y):
        if x == 0:
            return None
        return [int(i) for i in self.tree_lines[y][x-1::-1]]

    def right(self, x, y):
        if x == len(self.tree_lines[0]) - 1:
            return None
        return [int(i) for i in self.tree_lines[y][x+1:]]

    def height(self, x, y):
        return int(self.tree_lines[y][x])
        
    def check_visible(self, x, y):
        height = self.height(x, y) 
        return self.is_visible(height, self.up(x, y)) or self.is_visible(height, self.down(x, y)) or self.is_visible(height, self.left(x, y)) or self.is_visible(height, self.right(x, y))

    def scenic_score(self, x, y):
        height = self.height(x,y)
        return self.count_visible(height, self.up(x, y)) * self.count_visible(height, self.down(x, y)) * self.count_visible(height, self.left(x, y)) * self.count_visible(height, self.right(x, y))

    def total_trees_visible(self):
        count = 0
        for x in range(0, len(self.tree_lines[0])):
            for y in range(0, len(self.tree_lines)):
                if self.check_visible(x, y):
                    count = count + 1
        return count

    def max_scenic_score(self):
        score = 0
        for x in range(0, len(self.tree_lines[0])):
            for y in range(0, len(self.tree_lines)):
                temp_score = self.scenic_score(x, y)
                if temp_score > score:
                    score = temp_score
        return score
